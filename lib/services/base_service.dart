import 'dart:async';
import 'dart:developer';

import 'package:dac/model/base_record.dart';
import 'package:dac/repositories/base_repository.dart';
import 'package:flutter/foundation.dart';

import 'debuglog_service.dart';

abstract class BaseService<T extends BaseRecord, R extends BaseRepository<T>> {
  final _controller = StreamController<List<T>>.broadcast();
  List<T> records = [];

  Stream<List<T>> get stream => _controller.stream;

  final R repository;

  BaseService(this.repository) {
    log("Starting Service");
    _loadAll();
  }

  void _loadAll() {
    repository.getAll().then((records) {
      debug.i("Loaded ${T.toString()}: " + records.length.toString());
      this.records = records;
      _controller.sink.add(records);
    });
  }

  @protected
  void notifyAll() {
    repository.getAll().then((records) {
      this.records = records;
      _controller.sink.add(records);
    });
  }

  Future<List<T>> getAll() async {
    return repository.getAll();
  }

  Future<T> read(int id) async {
    return repository.read(id);
  }

  void close() {
    _controller.close();
  }

  Future<T> save(T record) async =>
      record.id != null ? _update(record) : _addNew(record);

  Future<T> _addNew(T record) {
    return repository.addToDatabase(record).then((value) {
      T newRecord = record.setId(value);
      debug.i("Added " + value.toString());
      notifyAll();
      return newRecord;
    });
  }

  Future<void> remove(int id) async {
    return repository.deleteWithId(id).then((value) {
      debug.i("Deleted ${T.toString()} $id.");
      notifyAll();
      return null;
    });
  }

  void removeAll() {
    repository.deleteAll();
    debug.i("Deleted all db of " + T.toString());
    notifyAll();
  }

  Future<T> _update(T record) {
    return repository.update(record).then((value) {
      debug.i("Updated " + record.toString());
      notifyAll();
      return record;
    });
  }

  Future<int> count() => repository.count();
}

Y cast<Y>(x) => x is Y ? x : null;

String formatDuration(Duration d) {
  var seconds = d.inSeconds;
  final days = seconds ~/ Duration.secondsPerDay;
  seconds -= days * Duration.secondsPerDay;
  final hours = seconds ~/ Duration.secondsPerHour;
  seconds -= hours * Duration.secondsPerHour;
  final minutes = seconds ~/ Duration.secondsPerMinute;
  seconds -= minutes * Duration.secondsPerMinute;

  final List<String> tokens = [];
  if (days != 0) {
    tokens.add('${days}d');
  }
  if (tokens.isNotEmpty || hours != 0) {
    tokens.add('${hours}h');
  }
  if (tokens.isNotEmpty || minutes != 0) {
    tokens.add('${minutes}m');
  }
  tokens.add('${seconds}s');

  return tokens.join(':');
}
