import 'package:dac/model/printer.dart';
import 'package:dac/services/printers_data_service.dart';
import 'package:dac/services/printers_service.dart';
import 'package:hooks_riverpod/all.dart';

enum ConnectionState { LOADING, LOADED, CLOSED, ERROR }

final connectionStateProvider = StateNotifierProvider.family(
    (ref, printer) => ConnectionStateNotifier(ref, printer));

class ConnectionStateNotifier extends StateNotifier<ConnectionState> {
  ConnectionStateNotifier(ProviderReference providerReference, Printer printer)
      : super(ConnectionState.CLOSED) {
    PrintersService().stream.listen((printers) {
      if (printers.contains(printer) &&
          !printers.firstWhere((element) => element == printer).enabled) {
        state = ConnectionState.CLOSED;
      }
    });
    PrintersDataService()
        .stream
        .where((p) => p.printer == printer)
        .listen((data) => state = ConnectionState.LOADED)
        .onError((e) {
      if (e.printer == printer) {
        state = ConnectionState.ERROR;
      }
    });
  }
}
