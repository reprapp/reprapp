import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

import 'base_record.dart';

class Webcam extends Equatable implements BaseRecord {
  final int id;
  final String url;
  final String name;
  final bool enabled;
  final WebcamFlip flip;
  final WebcamRotate rotate;

  @override
  List<Object> get props => [id, name];

  const Webcam({
    @required this.id,
    this.url,
    this.name,
    this.enabled,
    this.flip,
    this.rotate,
  });

  @override
  String toString() {
    return 'Webcam{id: $id, url: $url, name: $name, enabled: $enabled, flip: $flip, rotate: $rotate}';
  }

  Webcam copyWith({
    int id,
    String url,
    String name,
    bool enabled,
    WebcamFlip flip,
    WebcamRotate rotate,
  }) {
    if ((id == null || identical(id, this.id)) &&
        (url == null || identical(url, this.url)) &&
        (name == null || identical(name, this.name)) &&
        (enabled == null || identical(enabled, this.enabled)) &&
        (flip == null || identical(flip, this.flip)) &&
        (rotate == null || identical(rotate, this.rotate))) {
      return this;
    }

    return new Webcam(
      id: id ?? this.id,
      url: url ?? this.url,
      name: name ?? this.name,
      enabled: enabled ?? this.enabled,
      flip: flip ?? this.flip,
      rotate: rotate ?? this.rotate,
    );
  }

  @override
  Webcam setId(int id) {
    return copyWith(id: id);
  }
}

enum WebcamFlip { NONE, VERTICAL, HORIZONTAL, BOTH }

enum WebcamRotate { R0, R90, R180, R270 }
