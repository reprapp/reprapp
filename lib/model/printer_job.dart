import 'dart:convert';

import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class JobStatus extends Equatable {
  final Duration warmUpDuration;
  final JobLayer firstLayer;
  final JobLayer currentLayer;
  final JobProgress progress;
  final JobLeft jobLeft;

  const JobStatus({
    @required this.warmUpDuration,
    @required this.firstLayer,
    @required this.currentLayer,
    @required this.progress,
    @required this.jobLeft,
  });

  @override
  List<Object> get props =>
      [warmUpDuration, firstLayer, currentLayer, progress, jobLeft];

  @override
  String toString() {
    return 'JobStatus{warmUpDuration: $warmUpDuration, firstLayer: $firstLayer, currentLayer: $currentLayer, progress: $progress, jobLeft: $jobLeft}';
  }

  Map<String, dynamic> toMap() {
    // ignore: unnecessary_cast
    return {
      'warmUpDuration': this.warmUpDuration?.inMilliseconds,
      'firstLayer': this.firstLayer?.toMap(),
      'currentLayer': this.currentLayer?.toMap(),
      'progress': this.progress?.toMap(),
      'jobLeft': this.jobLeft?.toMap(),
    } as Map<String, dynamic>;
  }

  String toJson() => json.encode(this.toMap());
}

class JobFile {
  final String fileName;
  final String generatedBy;
  final int fileSize;
  final DateTime lastModified;
  final double totalHeight;
  final int totalLayers;
  final Duration totaDuration;
  final List<double> filamentsLength;

  const JobFile({
    @required this.fileName,
    @required this.generatedBy,
    @required this.fileSize,
    @required this.lastModified,
    @required this.totalHeight,
    @required this.totalLayers,
    @required this.totaDuration,
    @required this.filamentsLength,
  });
}

class JobLayer extends Equatable {
  final int layer;
  final double height;
  final Duration duration;

  const JobLayer({
    @required this.layer,
    @required this.height,
    @required this.duration,
  });

  @override
  List<Object> get props => [layer, height];

  Map<String, dynamic> toMap() {
    // ignore: unnecessary_cast
    return {
      'layer': this.layer,
      'height': this.height,
      'duration': this.duration,
    } as Map<String, dynamic>;
  }
}

class JobProgress extends Equatable {
  final double printedPercent;
  final Duration duration;
  final List<double> filamentsLength;
  final double filamentsTotalLength;

  JobProgress._({
    @required this.printedPercent,
    @required this.duration,
    @required this.filamentsLength,
    @required this.filamentsTotalLength,
  });

  factory JobProgress({
    @required double printedPercent,
    @required Duration duration,
    @required List<double> filamentsLength,
  }) {
    return JobProgress._(
        printedPercent: printedPercent,
        duration: duration,
        filamentsLength: filamentsLength,
        filamentsTotalLength: filamentsLength.reduce((a, b) => a + b));
  }

  @override
  List<Object> get props =>
      [printedPercent, duration, filamentsLength, filamentsTotalLength];

  Map<String, dynamic> toMap() {
    // ignore: unnecessary_cast
    return {
      'printedPercent': this.printedPercent,
      'duration': duration?.inMicroseconds,
      'filamentsLength': List<dynamic>.from(this.filamentsLength.map((x) => x)),
      'filamentsTotalLength': this.filamentsTotalLength,
    } as Map<String, dynamic>;
  }
}

class JobLeft extends Equatable {
  final Duration durationFile;
  final Duration durationFilament;
  final Duration durationLayer;

  const JobLeft({
    @required this.durationFile,
    @required this.durationFilament,
    @required this.durationLayer,
  });

  @override
  List<Object> get props => [durationFile, durationFilament, durationLayer];

  Map<String, dynamic> toMap() {
    // ignore: unnecessary_cast
    return {
      'durationFile': durationFile?.inMicroseconds,
      'durationFilament': durationFilament?.inMicroseconds,
      'durationLayer': durationLayer?.inMicroseconds,
    } as Map<String, dynamic>;
  }
}
