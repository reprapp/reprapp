import 'package:dac/model/status_type1.dart';
import 'package:dac/services/base_service.dart';
import 'package:meta/meta.dart';

StatusType3 statusType3FromJsonMap(Map<String, dynamic> map) =>
    StatusType3.fromJson(map);

class StatusType3 extends StatusType1 {
  StatusType3({
    @required String status,
    @required Coords coords,
    @required Speeds speeds,
    @required int currentTool,
    @required Params params,
    @required int seq,
    @required Sensors sensors,
    @required Temps temps,
    @required double time,
    @required this.currentLayer,
    @required this.currentLayerTime,
    @required this.extrRaw,
    @required this.fractionPrinted,
    @required this.filePosition,
    @required this.firstLayerDuration,
    @required this.firstLayerHeight,
    @required this.printDuration,
    @required this.warmUpDuration,
    @required this.timesLeft,
  }) : super(
            status: status,
            coords: coords,
            speeds: speeds,
            currentTool: currentTool,
            params: params,
            seq: seq,
            sensors: sensors,
            temps: temps,
            time: time);

  final int currentLayer;
  final double currentLayerTime;
  final List<double> extrRaw;
  final double fractionPrinted;
  final int filePosition;
  final double firstLayerDuration;
  final double firstLayerHeight;
  final double printDuration;
  final double warmUpDuration;
  final TimesLeft timesLeft;

  factory StatusType3.fromJson(Map<String, dynamic> json) => StatusType3(
        status: json["status"] == null ? null : json["status"],
        coords: json["coords"] == null ? null : Coords.fromJson(json["coords"]),
        speeds: json["speeds"] == null ? null : Speeds.fromJson(json["speeds"]),
        currentTool: json["currentTool"] == null ? null : json["currentTool"],
        params: json["params"] == null ? null : Params.fromJson(json["params"]),
        seq: json["seq"] == null ? null : json["seq"],
        sensors:
            json["sensors"] == null ? null : Sensors.fromJson(json["sensors"]),
        temps: json["temps"] == null ? null : Temps.fromJson(json["temps"]),
        time: cast<double>(json["time"]),
        currentLayer:
            json["currentLayer"] == null ? null : json["currentLayer"],
        currentLayerTime: cast<double>(json["currentLayerTime"]),
        extrRaw: json["extrRaw"] == null
            ? null
            : List<double>.from(json["extrRaw"].map((x) => x.toDouble())),
        fractionPrinted: cast<double>(json["fractionPrinted"]),
        filePosition:
            json["filePosition"] == null ? null : json["filePosition"],
        firstLayerDuration: cast<double>(json["firstLayerDuration"]),
        firstLayerHeight: cast<double>(json["firstLayerHeight"]),
        printDuration: cast<double>(json["printDuration"]),
        warmUpDuration: cast<double>(json["warmUpDuration"]),
        timesLeft: json["timesLeft"] == null
            ? null
            : TimesLeft.fromJson(json["timesLeft"]),
      );
}

class TimesLeft {
  TimesLeft({
    @required this.file,
    @required this.filament,
    @required this.layer,
  });

  final double file;
  final double filament;
  final double layer;

  factory TimesLeft.fromJson(Map<String, dynamic> json) => TimesLeft(
        file: cast<double>(json["file"]),
        filament: cast<double>(json["filament"]),
        layer: cast<double>(json["layer"]),
      );
}
