import 'package:dac/model/webcam.dart';
import 'package:dac/repositories/base_repository.dart';

class WebcamsRepository extends BaseRepository<Webcam> {
  static const String DB_TABLE = "Webcams";
  static const String COLUMN_URL = 'url';
  static const String COLUMN_NAME = 'name';
  static const String COLUMN_ENABLED = 'enabled';
  static const String COLUMN_FLIP = 'flip';
  static const String COLUMN_ROTATE = 'rotate';

  WebcamsRepository._() : super(DB_TABLE);

  static final WebcamsRepository db = WebcamsRepository._();

  Future<bool> existsUrl({String url}) async {
    return super.existsValue(column: COLUMN_URL, value: url);
  }

  Webcam fromMap(Map<String, dynamic> map) {
    return new Webcam(
      id: map[BaseRepository.COLUMN_ID] as int,
      url: map[COLUMN_URL] as String,
      name: map[COLUMN_NAME] as String,
      enabled: map[COLUMN_ENABLED] == 1,
      flip: map[COLUMN_FLIP] != null
          ? WebcamFlip.values[map[COLUMN_FLIP] as int]
          : WebcamFlip.NONE,
      rotate: map[COLUMN_ROTATE] != null
          ? WebcamRotate.values[map[COLUMN_ROTATE] as int]
          : WebcamRotate.R0,
    );
  }

  Map<String, dynamic> toMap(Webcam record) {
    var map = <String, dynamic>{
      COLUMN_URL: record.url,
      COLUMN_NAME: record.name,
      COLUMN_ENABLED: record.enabled ? 1 : 0,
      COLUMN_FLIP: record.flip?.index,
      COLUMN_ROTATE: record.rotate?.index,
    };
    if (record.id != null) {
      map[BaseRepository.COLUMN_ID] = record.id;
    }
    return map;
  }
}
