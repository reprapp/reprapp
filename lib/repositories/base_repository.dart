import 'dart:async';
import 'dart:developer';

import 'package:dac/model/base_record.dart';
import 'package:dac/services/base_service.dart';
import 'package:dac/services/debuglog_service.dart';
import 'package:flutter/foundation.dart';
import 'package:sqflite/sqflite.dart';

import 'database.dart';

abstract class BaseRepository<T extends BaseRecord> {
  static const String COLUMN_ID = '_id';
  final String dbTable;

  BaseRepository(this.dbTable);

  @protected
  int getIdFromResult(var map) {
    return cast<int>(map[BaseRepository.COLUMN_ID]);
  }

  Future<int> addToDatabase(T record) async {
    final db = await MainDatabase.db.database;
    var a = db.insert(dbTable, toMap(record),
        conflictAlgorithm: ConflictAlgorithm.replace);
    log("Saved in database: $dbTable " + record.toString());
    return a;
  }

  Future<T> read(int id) async {
    final db = await MainDatabase.db.database;
    var response =
        await db.query(dbTable, where: "$COLUMN_ID = ?", whereArgs: [id]);
    log("Database load id[$id] from: $dbTable ");
    List<T> list = [];
    response.forEach((r) {
      try {
        T record = fromMap(r);
        list.add(record);
      } catch (e) {
        debug.e("Error loading data from database: $e");
      }
    });
    return list.isNotEmpty ? list.first : throw Exception("Record not found");
  }

  Future<int> count() async {
    final db = await MainDatabase.db.database;
    int count = Sqflite.firstIntValue(
        await db.rawQuery('SELECT COUNT(*) FROM $dbTable'));
    return count;
  }

  Future<bool> exists({int id}) async {
    final db = await MainDatabase.db.database;
    var response =
        await db.query(dbTable, where: "$COLUMN_ID = ?", whereArgs: [id]);
    return response.isNotEmpty;
  }

  Future<bool> existsValue(
      {@required String column, @required var value}) async {
    final db = await MainDatabase.db.database;
    var response =
        await db.query(dbTable, where: "$column = ?", whereArgs: [value]);
    return response.isNotEmpty;
  }

  @protected
  List<T> loadRecords(var response) {
    List<T> list = [];
    response.forEach((r) {
      try {
        T record = fromMap(r);
        list.add(record);
      } catch (e) {
        debug.e(
            "Error loading data from database: $e \nDeleting faulty record: $r");
        deleteWithId(getIdFromResult(r)).then(
            (value) => debug.i("Deleted faulty record: $r"),
            onError: (ee) => debug.e("Error deleting faulty record: $ee"));
      }
    });
    return list;
  }

  Future<List<T>> getAll() async {
    final db = await MainDatabase.db.database;
    var response = await db.query(dbTable);
    List<T> list = loadRecords(response);
    log("Database load all from: $dbTable " + list.length.toString());
    return list;
  }

  Future<List<T>> getAllWhere(
      {@required String column, @required var value}) async {
    final db = await MainDatabase.db.database;
    var response =
        await db.query(dbTable, where: "$column = ?", whereArgs: [value]);
    List<T> list = loadRecords(response);
    log("Database load where from: $dbTable " + list.length.toString());
    return list;
  }

  Future<int> deleteWithId(int id) async {
    final db = await MainDatabase.db.database;
    log("Database delete from: $dbTable " + id.toString());
    return db.delete(dbTable, where: COLUMN_ID + "= ?", whereArgs: [id]);
  }

  void deleteAll() async {
    final db = await MainDatabase.db.database;
    db.delete(dbTable);
    log("Database delete all: $dbTable");
  }

  Future<int> update(T record) async {
    final db = await MainDatabase.db.database;
    var response = await db.update(dbTable, toMap(record),
        where: COLUMN_ID + " = ?",
        whereArgs: [record.id],
        conflictAlgorithm: ConflictAlgorithm.replace);
    log("Update in database: $dbTable " + record.toString());
    return response;
  }

  @protected
  T fromMap(Map<String, dynamic> map);

  @protected
  Map<String, dynamic> toMap(T record);
}
