import 'dart:developer';

import 'package:dac/model/printer.dart';
import 'package:dac/repositories/database.dart';
import 'package:flutter/foundation.dart';

import 'base_repository.dart';

class PrintersRepository extends BaseRepository<Printer> {
  static const String DB_TABLE = "Printers";
  static const String COLUMN_HOSTNAME = 'hostname';
  static const String COLUMN_NAME = 'name';
  static const String COLUMN_VERSION = 'version';
  static const String COLUMN_ENABLED = 'enabled';

  PrintersRepository._() : super(DB_TABLE);

  static final PrintersRepository db = PrintersRepository._();

  Future<bool> existsHostname({String hostname}) async {
    return super.existsValue(column: COLUMN_HOSTNAME, value: hostname);
  }

  Future<List<Printer>> getAllWithFilter({@required String filter}) async {
    final db = await MainDatabase.db.database;
    if (filter != null && filter.isNotEmpty) {
      var response = await db.query(dbTable,
          where: "$COLUMN_HOSTNAME LIKE ? OR $COLUMN_NAME LIKE ?",
          whereArgs: [filter, filter]);
      List<Printer> list = loadRecords(response);
      log("Database filter from: $dbTable " + list.length.toString());
      return list;
    } else {
      return getAll();
    }
  }

  Printer fromMap(Map<String, dynamic> map) {
    return new Printer(
      id: map[BaseRepository.COLUMN_ID] as int,
      hostname: map[COLUMN_HOSTNAME] as String,
      name: map[COLUMN_NAME] as String,
      version: map[COLUMN_VERSION] as int,
      enabled: map[COLUMN_ENABLED] == 1,
    );
  }

  Map<String, dynamic> toMap(Printer printer) {
    var map = <String, dynamic>{
      COLUMN_HOSTNAME: printer.hostname,
      COLUMN_NAME: printer.name,
      COLUMN_VERSION: printer.version,
      COLUMN_ENABLED: printer.enabled ? 1 : 0,
    };
    if (printer.id != null) {
      map[BaseRepository.COLUMN_ID] = printer.id;
    }
    return map;
  }
}
