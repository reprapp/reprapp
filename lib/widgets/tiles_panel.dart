import 'dart:math';

import 'package:dac/model/space.dart';
import 'package:dac/scaffolds/tiles_select_page.dart';
import 'package:dac/services/spaces_service.dart';
import 'package:dac/services/tiles_service.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:hooks_riverpod/all.dart';
import 'package:reorderables/reorderables.dart';

import 'tiles/tile_widget_base.dart';

final tileSizeProvider = StateProvider.family<double, TileMediaQuery>(
    (ref, tileMediaQuery) => _getTileSide(tileMediaQuery));

class TileMediaQuery extends Equatable {
  final MediaQueryData mediaQueryData;

  TileMediaQuery(this.mediaQueryData);

  factory TileMediaQuery.context(BuildContext context) {
    return TileMediaQuery(MediaQuery.of(context));
  }

  @override
  List<Object> get props =>
      [mediaQueryData.size.width, mediaQueryData.orientation];
}

double _getTileSide(TileMediaQuery tileMediaQuery) {
  double width = tileMediaQuery.mediaQueryData.size.width;
  double side = _getFitSide(
      width,
      tileMediaQuery.mediaQueryData.orientation == Orientation.landscape
          ? TILES_IDEAL_EXTENT_LANDSCAPE
          : TILES_IDEAL_EXTENT_PORTRAIT);

  int fit = (width - TILE_SPACING) ~/ (side + TILE_SPACING);

  double sideFit = (width - TILE_SPACING * 2) / fit - TILE_SPACING;

  print("fit side");
  return sideFit;
}

double _getFitSide(double size, int extent) {
  double insideSize = size - TILE_SPACING * 2;
  double totalBetweenSpacing = TILE_SPACING * (extent - 1);
  double fitSide = (insideSize - totalBetweenSpacing) / extent;
  return min(max(fitSide, TILE_SIZE_MIN), TILE_SIZE_MAX);
}

class SpaceTilesLive extends ConsumerWidget {
  final Space space;

  SpaceTilesLive({@required this.space});

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    List<SpaceTile> spaceTiles = watch(spaceTilesListProvider(space)).records;

    List<TileSpace> _tiles = [];
    _tiles.addAll(spaceTiles
        .map((e) => tileFactorySpace(space: space, spaceTile: e, isEdit: false))
        .toList());

    return _tiles.length > 0
        ? SingleChildScrollView(
            child: Padding(
                padding: EdgeInsets.all(TILE_SPACING),
                child: Wrap(
                  spacing: TILE_SPACING,
                  runSpacing: TILE_SPACING,
                  alignment: WrapAlignment.start,
                  runAlignment: WrapAlignment.center,
                  children: _tiles,
                )))
        : Container(
            padding: EdgeInsets.all(50),
            child: Center(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text("Add a tile to this empty Space"),
                    Container(
                      height: 30,
                    ),
                    FloatingActionButton(
                        child: Icon(Icons.add),
                        onPressed: () => Navigator.of(context).push(
                            MaterialPageRoute(
                                builder: (_) => TilesSelectPage(space),
                                fullscreenDialog: true)))
                  ]),
            ),
          );
  }
}

class SpaceTilesEdit extends ConsumerWidget {
  final Space space;

  SpaceTilesEdit({@required this.space});

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    List<SpaceTile> spaceTiles = watch(spaceTilesListProvider(space)).records;

    return _SpaceTilesEdit(space: space, spaceTiles: spaceTiles);
  }
}

class _SpaceTilesEdit extends StatefulWidget {
  final Space space;
  final List<SpaceTile> spaceTiles;

  _SpaceTilesEdit({this.space, this.spaceTiles});

  @override
  _SpaceTilesEditState createState() => _SpaceTilesEditState(space, spaceTiles);
}

class _SpaceTilesEditState extends State<_SpaceTilesEdit> {
  final Space space;

  List<SpaceTile> spaceTiles;

  _SpaceTilesEditState(this.space, this.spaceTiles);

  List<TileSpace> _tiles = [];

  @override
  void initState() {
    super.initState();
    _tiles.addAll(spaceTiles
        .map((e) => tileFactorySpace(space: space, spaceTile: e, isEdit: false))
        .toList());
  }

  void _setTiles(List<SpaceTile> spaceTiles) {
    _tiles.clear();
    _tiles.addAll(spaceTiles
        .map((e) => tileFactorySpace(space: space, spaceTile: e, isEdit: true))
        .toList());
  }

  @override
  Widget build(BuildContext context) {
    _setTiles(context.read(spaceTilesListProvider(space)).records);
    void _onReorder(int oldIndex, int newIndex) {
      setState(() {
        TileSpace move = _tiles.removeAt(oldIndex);
        _tiles.insert(newIndex, move);
        context
            .read(spaceTilesListProvider(space))
            .saveLocations(_tiles.map((e) => e.spaceTile).toList());
      });
    }

    return SingleChildScrollView(
        child: ReorderableWrap(
      spacing: TILE_SPACING,
      runSpacing: TILE_SPACING,
      padding: EdgeInsets.all(TILE_SPACING),
      children: _tiles,
      onReorder: _onReorder,
      needsLongPressDraggable: false,
    ));
  }
}
