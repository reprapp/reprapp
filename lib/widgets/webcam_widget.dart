import 'package:dac/model/webcam.dart';
import 'package:dac/services/webcams_service.dart';
import 'package:dac/widgets/video_widget.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_vlc_player/flutter_vlc_player.dart';
import 'package:hooks_riverpod/all.dart';

class WebcamViewWidget extends StatefulWidget {
  final Webcam webcam;

  WebcamViewWidget(this.webcam);

  @override
  _WebcamVideoState createState() => _WebcamVideoState(webcam);
}

class _WebcamVideoState extends State<WebcamViewWidget> {
  _WebcamVideoState(this.webcam);

  final Webcam webcam;

  VlcPlayerController _videoViewController;
  String _url;
  var _disposeListener;
  bool _isPlaying = false;

  @override
  void initState() {
    _videoViewController = new VlcPlayerController(onInit: () {
      _videoViewController.play();
      _isPlaying = true;
    });
    _disposeListener =
        context.read(webcamProvider(webcam)).addListener((state) {
      if (_isPlaying) {
        _videoViewController.setStreamUrl(state.url);
        _videoViewController.setVolume(0);
      }
    });
    _url = webcam.url;
    super.initState();
  }

  @override
  void dispose() {
    _disposeListener();
    _videoViewController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Expanded(child: Center(child: VideoWidget(_videoViewController, _url))),
      ],
    );
  }
}
