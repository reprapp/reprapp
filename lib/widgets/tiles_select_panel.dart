import 'dart:math';

import 'package:dac/services/tiles_service.dart';
import 'package:dac/widgets/tiles_panel.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:hooks_riverpod/all.dart';

import 'tiles/tile_widget_base.dart';

class TilesPanelSelect extends ConsumerWidget {
  final List<TileSelect> tiles;

  TilesPanelSelect({@required this.tiles});

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    double tilePaintSize =
        watch(tileSizeProvider(TileMediaQuery.context(context))).state;

    int maxHeight =
        tiles.isNotEmpty ? tiles.map((e) => e.tileSize.y).reduce(max) : 0;

    List<Widget> items = [_ItemSpacer()];
    items.addAll(tiles.map((e) => _SelectableTile(e, tilePaintSize)).toList());
    items.add(_ItemSpacer());

    return Container(
        height: maxHeight * (tilePaintSize + TILE_SPACING * 4),
        // margin: EdgeInsets.symmetric(vertical: spacing * 2),
        child: ListView(
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            children: items));
  }
}

class _ItemSpacer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(width: TILE_SPACING * 2);
  }
}

class _SelectableTile extends ConsumerWidget {
  final TileSelect tile;
  final double tilePaintSize;

  _SelectableTile(this.tile, this.tilePaintSize);

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    SelectedTileEvent selectedTile = watch(tileSelectedProvider.state);

    bool selected = selectedTile.selected == tile;
    return Container(
        alignment: Alignment.topCenter,
        child: Container(
          width:
              tilePaintSide(tile.tileSize.x, tilePaintSize) + TILE_SPACING * 2,
          height:
              tilePaintSide(tile.tileSize.y, tilePaintSize) + TILE_SPACING * 2,
          alignment: Alignment.center,
          padding: EdgeInsets.all(TILE_SPACING),
          // margin: EdgeInsets.all(spacing),
          decoration: BoxDecoration(
            color: selected ? Theme.of(context).indicatorColor : null,
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
          ),
          child: GestureDetector(
            child: AbsorbPointer(absorbing: true, child: tile),
            onTap: () {
              HapticFeedback.selectionClick();
              context.read(tileSelectedProvider).select(selected ? null : tile);
            },
          ),
        ));
  }
}
