import 'package:dac/model/printer.dart';
import 'package:dac/services/printer_communication_providers.dart' as comm;
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:hooks_riverpod/all.dart';

class PrinterConnectionIcon extends ConsumerWidget {
  final Printer printer;
  final double size;

  PrinterConnectionIcon({@required this.printer, this.size = 20});

  Widget build(BuildContext context, ScopedReader watch) {
    comm.ConnectionState connectionState =
        watch(comm.connectionStateProvider(printer).state);

    return ConnectionIcon(
      connectionState: connectionState,
      size: size,
    );
  }
}

class ConnectionIcon extends StatelessWidget {
  final double size;
  final comm.ConnectionState connectionState;

  ConnectionIcon({@required this.connectionState, this.size = 20});

  Widget build(BuildContext context) {
    Widget icon;
    var color = Theme.of(context).textTheme.caption.color;
    switch (connectionState) {
      case comm.ConnectionState.LOADING:
        icon = Icon(
          Icons.wifi,
          color: Colors.blue,
          size: size,
        );
        break;
      case comm.ConnectionState.LOADED:
        icon = Icon(Icons.wifi, size: size);
        break;
      case comm.ConnectionState.ERROR:
        color = Theme.of(context).colorScheme.error;
        icon = Icon(Icons.wifi_off, color: Colors.red, size: size);
        break;
      case comm.ConnectionState.CLOSED:
        icon = Icon(Icons.wifi_off, color: color, size: size);
        break;
    }

    return icon;
  }
}
