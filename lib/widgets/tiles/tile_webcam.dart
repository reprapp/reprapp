import 'package:dac/model/space.dart';
import 'package:dac/model/webcam.dart';
import 'package:dac/scaffolds/webcam_page.dart';
import 'package:dac/services/tiles_service.dart';
import 'package:dac/services/webcams_service.dart';
import 'package:dac/widgets/tiles/tile_webcam_settings.dart';
import 'package:dac/widgets/tiles/tile_widget_base.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_vlc_player/flutter_vlc_player.dart';
import 'package:hooks_riverpod/all.dart';

import '../video_widget.dart';

class TileWebcamSelect extends TileSelect {
  TileWebcamSelect._(TileSize tileSize, WebcamSpaceTileSettings settings)
      : super(
          tileKey: TileKey.CAM,
          tileSize: tileSize,
          widget: _TileStatic(settings),
          tileSettings: TileWebcamSettings(settings),
        );

  factory TileWebcamSelect(TileSize tileSize) {
    return TileWebcamSelect._(tileSize, WebcamSpaceTileSettings.static());
  }
}

class TileWebcamSpace extends TileSpace {
  TileWebcamSpace._(Space space, SpaceTile spaceTile,
      WebcamSpaceTileSettings settings, bool isEdit)
      : super(
          space: space,
          spaceTile: spaceTile,
          widgetLive: _TileLive(settings),
          widgetEdit: _TileStatic(settings),
          isEdit: isEdit,
          tileSettings: TileWebcamSettings(settings),
        );

  factory TileWebcamSpace(Space space, SpaceTile spaceTile, bool isEdit) {
    return TileWebcamSpace._(space, spaceTile,
        WebcamSpaceTileSettings(spaceTile.tileSettings), isEdit);
  }
}

class _TileStatic extends ConsumerWidget {
  final WebcamSpaceTileSettings settings;

  _TileStatic(this.settings);

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    AsyncValue<Webcam> webcam = watch(webcamProviderFromId(settings.webcamId));
    return webcam.when(
      data: (data) =>
          Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
        Expanded(
          child: Container(
              color: Theme.of(context).primaryColor,
              child: Icon(
                Icons.photo_camera,
                size: 30,
              )),
        ),
        Text(
          data?.name ?? "Webcam",
          overflow: TextOverflow.ellipsis,
          style: Theme.of(context).textTheme.subtitle1,
        ),
        Text(data?.url ?? "IP Stream",
            overflow: TextOverflow.ellipsis,
            style: Theme.of(context).textTheme.caption)
      ]),
      loading: () => const CircularProgressIndicator(),
      error: (err, stack) => Text('Error: $err'),
    );
  }
}

class _TileLive extends ConsumerWidget {
  final WebcamSpaceTileSettings settings;

  _TileLive(this.settings);

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    AsyncValue<Webcam> webcam = watch(webcamProviderFromId(settings.webcamId));
    return webcam.when(
      data: (data) => _VideoWidget(data),
      loading: () => const CircularProgressIndicator(),
      error: (err, stack) => Text('Error: $err'),
    );
  }
}

class _VideoWidget extends StatefulWidget {
  const _VideoWidget(this.record);

  final Webcam record;

  @override
  _WebcamVideoState createState() => _WebcamVideoState(record);
}

class _WebcamVideoState extends State<_VideoWidget> {
  _WebcamVideoState(this.webcam);

  final Webcam webcam;

  VlcPlayerController _videoViewController;
  String _url;

  @override
  void initState() {
    _videoViewController = new VlcPlayerController(onInit: () {
      _videoViewController.play();
    });
    _url = webcam.url;
    super.initState();
  }

  @override
  void dispose() {
    _videoViewController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        _videoViewController.stop();
        Navigator.pushNamed(context, WebcamPage.routeName, arguments: webcam)
            .then((value) {
          _videoViewController.play();
        });
      },
      child: Center(child: VideoWidget(_videoViewController, _url)),
    );
  }
}
