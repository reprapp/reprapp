import 'package:dac/model/space.dart';
import 'package:dac/model/webcam.dart';
import 'package:dac/scaffolds/webcam_edit_page.dart';
import 'package:dac/services/spaces_service.dart';
import 'package:dac/services/webcams_service.dart';
import 'package:dac/widgets/tiles/tile_widget_base.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:hooks_riverpod/all.dart';
import 'package:riverpod/src/framework.dart';

import '../webcam_widget.dart';

class TileWebcamSettings extends TileSettings {
  TileWebcamSettings(WebcamSpaceTileSettings settings)
      : super(settingsWidget: _TileSettingsWidget(settings), title: "Webcam");
}

class WebcamSpaceTileSettings extends HasSpaceTileSettings {
  final int webcamId;
  final String label;

  WebcamSpaceTileSettings._({
    this.webcamId,
    @required this.label,
  });

  factory WebcamSpaceTileSettings.static() {
    return WebcamSpaceTileSettings._(label: "Webcam");
  }

  factory WebcamSpaceTileSettings(SpaceTileSettings settings) {
    return WebcamSpaceTileSettings._(
        webcamId: settings.properties["webcamId"] != null
            ? int.tryParse(settings.properties["webcamId"])
            : null,
        label: settings.properties["label"]);
  }

  SpaceTileSettings spaceTileSettings() {
    return SpaceTileSettings.create(
        {"webcamId": webcamId.toString(), "label": label});
  }

  WebcamSpaceTileSettings copyWith({
    int webcamId,
    String label,
  }) {
    if ((webcamId == null || identical(webcamId, this.webcamId)) &&
        (label == null || identical(label, this.label))) {
      return this;
    }

    return WebcamSpaceTileSettings._(
      webcamId: webcamId ?? this.webcamId,
      label: label ?? this.label,
    );
  }
}

final _webcamSelectProvider = ChangeNotifierProvider.autoDispose(
    (ref) => _WebcamSelectChangeNotifier(ref));

class _WebcamSelectChangeNotifier extends ChangeNotifier {
  _SelectableItem selected;

  final ProviderReference ref;

  _WebcamSelectChangeNotifier(this.ref);

  _SelectableItem setSelected(_SelectableItem select) {
    if (selected != null) selected.isSelected = false;
    select.isSelected = true;
    selected = select;
    return selected;
  }

  void changeSelected(
      WebcamSpaceTileSettings settings, _SelectableItem select) {
    ref.read(spaceTileSettingsNotifierProvider).change(
        settings.copyWith(webcamId: selected.webcam.id).spaceTileSettings());
    setSelected(select);
    notifyListeners();
  }
}

class _TileSettingsWidget extends ConsumerWidget {
  final WebcamSpaceTileSettings initialSettings;

  _TileSettingsWidget(this.initialSettings);

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    List<_SelectableItem> _records = [];
    bool _selected = false;

    watch(webcamsListProvider).records?.forEach((w) {
      _SelectableItem item = _SelectableItem(
          webcam: w,
          isSelected: initialSettings.webcamId != null &&
              w.id == initialSettings.webcamId);
      if (item.isSelected) {
        context.read(_webcamSelectProvider).setSelected(item);
        _selected = true;
      }
      _records.add(item);
    });
    if (_records.length > 0) {
      if (!_selected) {
        context.read(_webcamSelectProvider).setSelected(_records.first);
      }
      return Center(
          child: _TileSettingsSelectWebcam(initialSettings, _records));
    } else {
      return Center(
        child: Container(
          padding: EdgeInsets.all(30),
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
            Text("You don't have any webcam :("),
            Text("Add a new webcam"),
            Container(
              height: 30,
            ),
            FloatingActionButton(
              onPressed: () => Navigator.push(context,
                  MaterialPageRoute(builder: (context) => WebcamEditPage())),
              child: Icon(Icons.add),
            ),
          ]),
        ),
      );
    }
  }
}

class _TileSettingsSelectWebcam extends ConsumerWidget {
  final WebcamSpaceTileSettings settings;
  final List<_SelectableItem> records;

  _TileSettingsSelectWebcam(this.settings, this.records);

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    _SelectableItem selected = watch(_webcamSelectProvider).selected;

    // watch(spaceTileSettingsProvider).setSettings(settings
    //     .copyWith(webcamId: selected.webcam.id, label: selected.webcam.name)
    //     .spaceTileSettings());

    return _TileSettingsEditWidget(settings, records, selected);
  }
}

class _TileSettingsEditWidget extends StatelessWidget {
  final WebcamSpaceTileSettings settings;
  final List<_SelectableItem> records;
  final _SelectableItem initialSelected;

  _TileSettingsEditWidget(this.settings, this.records, this.initialSelected);

  @override
  Widget build(BuildContext context) {
    List<Widget> items = [];
    records.forEach((a) {
      items.add(_listTile(context, a,
          onSelect: (e) =>
              context.read(_webcamSelectProvider).changeSelected(settings, e)));
    });
    // TODO fix this
    return GridView.extent(
      padding: const EdgeInsets.all(20),
      crossAxisSpacing: 10,
      mainAxisSpacing: 10,
      maxCrossAxisExtent: 200.0,
      shrinkWrap: true,
      scrollDirection: Axis.vertical,
      children: items,
    );
  }

  Widget _listTile(BuildContext context, _SelectableItem webcamItem,
      {Function onSelect}) {
    return InkWell(
        onTap: () => onSelect(webcamItem),
        child: Container(
            width: 200,
            height: 200,
            padding: EdgeInsets.all(15),
            // margin: EdgeInsets.all(15),
            // color: webcamItem.isSelected ? Colors.white54 : Colors.transparent,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(15)),
                border: Border.all(
                    width: 3,
                    color: webcamItem.isSelected
                        ? Theme.of(context).primaryColor
                        : Colors.transparent)),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: [
                Expanded(child: WebcamViewWidget(webcamItem.webcam)),
                Text(webcamItem.webcam.name)
              ],
            )));
  }
}

class _SelectableItem {
  bool isSelected = false;
  Webcam webcam;

  _SelectableItem({
    @required this.isSelected,
    @required this.webcam,
  });

  _SelectableItem copyWith({
    bool isSelected,
    Webcam webcam,
  }) {
    return new _SelectableItem(
      isSelected: isSelected ?? this.isSelected,
      webcam: webcam ?? this.webcam,
    );
  }
}
