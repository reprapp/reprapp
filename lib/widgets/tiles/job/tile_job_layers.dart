import 'package:dac/model/printer.dart';
import 'package:dac/model/printer_job.dart';
import 'package:dac/model/space.dart';
import 'package:dac/services/printer_data_providers.dart';
import 'package:dac/services/printers_service.dart';
import 'package:dac/services/tiles_service.dart';
import 'package:dac/widgets/tiles/tile_printer_settings.dart';
import 'package:dac/widgets/tiles/tile_widget_base.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:hooks_riverpod/all.dart';

class TileJobLayersSelect extends TileSelect {
  TileJobLayersSelect._(TileSize tileSize, PrinterSpaceTileSettings settings)
      : super(
          tileKey: TileKey.PRINTER_JOB_LAYERS,
          tileSize: tileSize,
          widget: _TileStatic(settings, tileSize),
          tileSettings: TilePrinterSettings(settings),
        );

  factory TileJobLayersSelect(TileSize tileSize) {
    return TileJobLayersSelect._(tileSize, PrinterSpaceTileSettings.static());
  }
}

class TileJobLayersSpace extends TileSpace {
  TileJobLayersSpace._(Space space, SpaceTile spaceTile,
      PrinterSpaceTileSettings settings, bool isEdit)
      : super(
          space: space,
          spaceTile: spaceTile,
          widgetLive: _TileLive(settings, spaceTile.tileSize),
          widgetEdit: _TileStatic(settings, spaceTile.tileSize),
          isEdit: isEdit,
          tileSettings: TilePrinterSettings(settings),
        );

  factory TileJobLayersSpace(Space space, SpaceTile spaceTile, bool isEdit) {
    return TileJobLayersSpace._(space, spaceTile,
        PrinterSpaceTileSettings(spaceTile.tileSettings), isEdit);
  }
}

class _TileStatic extends StatelessWidget {
  final PrinterSpaceTileSettings settings;
  final TileSize tileSize;

  _TileStatic(this.settings, this.tileSize);

  @override
  Widget build(BuildContext context) {
    return _ChartLayersBase(
      maxHeight: 300.5,
      maxLayer: 2095,
      currentLayer: 836,
      currentHeight: 119.22,
      percent: 39.74,
    );
  }
}

class _TileLive extends ConsumerWidget {
  final PrinterSpaceTileSettings settings;
  final TileSize tileSize;

  _TileLive(this.settings, this.tileSize);

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    Printer printer = watch(printerProviderFromId(settings.printerId));
    return _TileLivePrinter(printer, tileSize);
  }
}

class _TileLivePrinter extends ConsumerWidget {
  final Printer printer;
  final TileSize tileSize;

  _TileLivePrinter(this.printer, this.tileSize);

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    JobFile jobFile = watch(printerJobFileNotifierProvider(printer)).jobFile;
    JobStatus jobStatus = watch(printerJobStatusProvider(printer).state);
    return _ChartLayersBase(
      maxLayer: jobFile?.totalLayers,
      maxHeight: jobFile?.totalHeight,
      currentLayer: jobStatus?.currentLayer?.layer,
      currentHeight: jobStatus?.currentLayer?.height,
      percent: jobStatus?.progress?.printedPercent,
    );
  }
}

class _ChartLayersBase extends StatelessWidget {
  final int currentLayer;
  final int maxLayer;
  final double currentHeight;
  final double maxHeight;
  final double percent;

  _ChartLayersBase({
    @required this.currentLayer,
    @required this.maxLayer,
    @required this.currentHeight,
    @required this.maxHeight,
    @required this.percent,
  });

  @override
  Widget build(BuildContext context) {
    double barWidth = 40;
    double fontSize = 10;
    Color color = Colors.red;

    var labelTextStyle = Theme.of(context)
        .primaryTextTheme
        .bodyText1
        .copyWith(fontSize: fontSize);

    return AspectRatio(
      aspectRatio: 1,
      child: Container(
        // color: Colors.red,
        child: Stack(
          children: [
            Padding(
              padding:
                  const EdgeInsets.only(top: 0, bottom: 30, left: 0, right: 0),
              child: BarChart(
                BarChartData(
                  groupsSpace: 0,
                  alignment: BarChartAlignment.center,
                  maxY: 100,
                  minY: 0,
                  titlesData: FlTitlesData(show: false),
                  gridData: FlGridData(show: false),
                  borderData: FlBorderData(show: false),
                  barGroups: [
                    BarChartGroupData(
                      x: 0,
                      barRods: [
                        BarChartRodData(
                          colors: [color],
                          y: percent ?? 0,
                          width: barWidth,
                          borderRadius: const BorderRadius.only(
                              topLeft: Radius.circular(1),
                              topRight: Radius.circular(0)),
                          backDrawRodData: BackgroundBarChartRodData(
                            show: true,
                            y: 100,
                            colors: [color.withAlpha(30)],
                          ),
                        ),
                      ],
                      showingTooltipIndicators: [0],
                    ),
                    BarChartGroupData(
                      x: 1,
                      barRods: [
                        BarChartRodData(
                          colors: [color],
                          y: percent ?? 0,
                          width: barWidth,
                          borderRadius: const BorderRadius.only(
                              topLeft: Radius.circular(0),
                              topRight: Radius.circular(1)),
                          backDrawRodData: BackgroundBarChartRodData(
                            show: true,
                            y: 100,
                            colors: [color.withAlpha(30)],
                          ),
                        ),
                      ],
                      showingTooltipIndicators: [0],
                    ),
                  ],
                  barTouchData: BarTouchData(
                    enabled: false,
                    touchTooltipData: BarTouchTooltipData(
                      tooltipBgColor: Colors.transparent,
                      tooltipPadding: const EdgeInsets.all(0),
                      tooltipBottomMargin: (percent ?? 0) > 50 ? -20 : 0,
                      fitInsideVertically: true,
                      getTooltipItem: (group, groupIndex, rod, rodIndex) {
                        return BarTooltipItem(
                          groupIndex == 0
                              ? currentLayer?.toString() ?? "0"
                              : _formatLabelHeight(currentHeight),
                          labelTextStyle,
                        );
                      },
                    ),
                  ),
                ),
              ),
            ),
            Positioned.directional(
              textDirection: TextDirection.ltr,
              bottom: 0,
              start: 0,
              child: Column(
                children: [
                  Text(
                    "Layers",
                    style: Theme.of(context)
                        .textTheme
                        .caption
                        .copyWith(fontSize: 10),
                  ),
                  Text(
                    maxLayer?.toStringAsFixed(0) ?? "0",
                    style: Theme.of(context)
                        .textTheme
                        .bodyText1
                        .copyWith(fontSize: 10),
                  ),
                ],
              ),
            ),
            Positioned.directional(
              textDirection: TextDirection.ltr,
              bottom: 0,
              end: 0,
              child: Column(children: [
                Text(
                  "Z mm",
                  style: Theme.of(context)
                      .textTheme
                      .caption
                      .copyWith(fontSize: 10),
                ),
                Text(
                  maxHeight?.toStringAsFixed(2) ?? "0.00",
                  textAlign: TextAlign.right,
                  style: Theme.of(context)
                      .textTheme
                      .bodyText1
                      .copyWith(fontSize: 10),
                ),
              ]),
            ),
          ],
        ),
      ),
    );
  }

  String _formatLabelHeight(double value) {
    return value?.toStringAsFixed(2) ?? "0.00";
  }
}
