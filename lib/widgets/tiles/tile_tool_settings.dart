import 'package:dac/model/printer.dart';
import 'package:dac/model/printer_data.dart';
import 'package:dac/model/space.dart';
import 'package:dac/services/printer_data_providers.dart';
import 'package:dac/services/printers_service.dart';
import 'package:dac/services/spaces_service.dart';
import 'package:dac/widgets/tiles/tile_printer_settings.dart';
import 'package:dac/widgets/tiles/tile_widget_base.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:hooks_riverpod/all.dart';

import '../printer_selector.dart';
import '../temperature_chart.dart';

class TileToolSettings extends TileSettings {
  TileToolSettings(SpaceTilePrinterToolSettings settings)
      : super(settingsWidget: _TileSettingsWidget(settings), title: "Printer");
}

abstract class HasPrinterToolSettings extends HasPrinterSettings {
  int get toolNumber;
  int get heaterNumber;
}

class SpaceTilePrinterToolSettings extends PrinterSpaceTileSettings
    implements HasPrinterToolSettings {
  final int toolNumber;
  final int heaterNumber;

  SpaceTilePrinterToolSettings.settings({
    int printerId,
    this.toolNumber,
    this.heaterNumber,
    String label,
  }) : super.settings(printerId: printerId, label: label);

  factory SpaceTilePrinterToolSettings.static() {
    return SpaceTilePrinterToolSettings.settings(label: "Tool");
  }

  factory SpaceTilePrinterToolSettings(SpaceTileSettings settings) {
    return SpaceTilePrinterToolSettings.settings(
      printerId: settings.getOrFunctionValue("printerId", int.tryParse, null),
      toolNumber: settings.getOrFunctionValue("toolNumber", int.tryParse, null),
      heaterNumber:
          settings.getOrFunctionValue("heaterNumber", int.tryParse, null),
      label: settings.getOrValue("label", null),
    );
  }

  SpaceTileSettings spaceTileSettings() {
    return SpaceTileSettings.create({
      "printerId": printerId.toString(),
      "toolNumber": toolNumber.toString(),
      "heaterNumber": heaterNumber.toString(),
      "label": label
    });
  }

  SpaceTilePrinterToolSettings copyWith({
    int printerId,
    int toolNumber,
    int heaterNumber,
    String label,
  }) {
    if ((printerId == null || identical(printerId, this.printerId)) &&
        (toolNumber == null || identical(toolNumber, this.toolNumber)) &&
        (heaterNumber == null || identical(heaterNumber, this.heaterNumber)) &&
        (label == null || identical(label, this.label))) {
      return this;
    }

    return new SpaceTilePrinterToolSettings.settings(
      printerId: printerId ?? this.printerId,
      toolNumber: toolNumber ?? this.toolNumber,
      heaterNumber: heaterNumber ?? this.heaterNumber,
      label: label ?? this.label,
    );
  }
}

class _TileSettingsWidget extends ConsumerWidget {
  final SpaceTilePrinterToolSettings initialSettings;
  _TileSettingsWidget(this.initialSettings);

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    SpaceTilePrinterToolSettings settings;
    if (initialSettings.printerId == null) {
      var record = watch(printersListProvider).records.first;
      settings =
          initialSettings.copyWith(printerId: record.id, label: record.name);
    } else {
      settings = initialSettings;
    }
    context
        .read(spaceTileSettingsNotifierProvider)
        .load(settings.spaceTileSettings());

    return Padding(
      padding: EdgeInsets.all(20),
      child: Column(
        children: [
          PrinterSelector(settings.printerId, (Printer p) {
            context.read(spaceTileSettingsNotifierProvider).change(settings
                .copyWith(
                    printerId: p.id,
                    label: p.name,
                    toolNumber: null,
                    heaterNumber: null)
                .spaceTileSettings());
          }),
          _ToolSettingsWidget(),
        ],
      ),
    );
  }
}

class _ToolSettingsWidget extends ConsumerWidget {
  @override
  Widget build(BuildContext context, ScopedReader watch) {
    SpaceTilePrinterToolSettings initialSettings = SpaceTilePrinterToolSettings(
        watch(spaceTileSettingsNotifierProvider).selected);

    if (initialSettings.printerId != null) {
      Printer printer = watch(printerProviderFromId(initialSettings.printerId));
      PrinterInfo printerInfo = watch(printerInfoProvider(printer)).state;

      ToolInfo selectedTool;
      if (initialSettings.toolNumber != null && printerInfo?.tools != null) {
        selectedTool = printerInfo.tools.firstWhere(
            (element) => element.number == initialSettings.toolNumber,
            orElse: () => null);
      }

      if (selectedTool == null && printerInfo?.tools?.first != null) {
        selectedTool = printerInfo.tools.first;

        context.read(spaceTileSettingsNotifierProvider).load(initialSettings
            .copyWith(toolNumber: selectedTool.number, heaterNumber: null)
            .spaceTileSettings());
      }

      if (selectedTool != null) {
        return Padding(
          padding: EdgeInsets.only(top: 40),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Tool",
                style: Theme.of(context).textTheme.subtitle1,
              ),
              _toolSelector(context, printerInfo?.tools, selectedTool,
                  (ToolInfo p) {
                context.read(spaceTileSettingsNotifierProvider).change(
                    initialSettings
                        .copyWith(toolNumber: p.number, heaterNumber: null)
                        .spaceTileSettings());
              }),
              _HeaterSettingsWidget(selectedTool),
            ],
          ),
        );
      } else {
        return Container();
      }
    } else {
      return Container();
    }
  }

  Widget _toolSelector(BuildContext context, List<ToolInfo> records,
      ToolInfo selected, Function onChanged) {
    List<Widget> items = [];

    records?.forEach((element) {
      items.add(ListTile(
        leading: Radio<ToolInfo>(
          value: element,
          groupValue: selected,
          onChanged: (ToolInfo value) {
            onChanged(value);
          },
        ),
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(element.name ?? "Tool " + element.number.toString(),
                style: Theme.of(context).textTheme.subtitle1),
            Text(
                "T" +
                    element.number.toString() +
                    (" - " + (selected?.filament ?? "")),
                style: Theme.of(context).textTheme.caption),
          ],
        ),
      ));
    });
    return Column(
      children: items,
    );
  }
}

class _HeaterSettingsWidget extends ConsumerWidget {
  final ToolInfo tool;
  _HeaterSettingsWidget(this.tool);

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    SpaceTilePrinterToolSettings initialSettings = SpaceTilePrinterToolSettings(
        watch(spaceTileSettingsNotifierProvider).selected);

    int selectedHeater = initialSettings.heaterNumber;
    if (!tool.heaters.contains(selectedHeater)) {
      selectedHeater = null;
    }

    if (selectedHeater == null) {
      selectedHeater = tool.heaters.first;

      context.read(spaceTileSettingsNotifierProvider).load(initialSettings
          .copyWith(heaterNumber: selectedHeater)
          .spaceTileSettings());
    }

    return Padding(
      padding: EdgeInsets.only(top: 20),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Heater",
            style: Theme.of(context).textTheme.subtitle1,
          ),
          _heaterSelector(context, tool.heaters, selectedHeater, (int p) {
            context.read(spaceTileSettingsNotifierProvider).change(
                initialSettings.copyWith(heaterNumber: p).spaceTileSettings());
          }),
        ],
      ),
    );
  }

  Widget _heaterSelector(BuildContext context, List<int> records, int selected,
      Function onChanged) {
    List<Widget> items = [];

    records.forEach((element) {
      items.add(ListTile(
        leading: Radio<int>(
          value: element,
          groupValue: selected,
          onChanged: (int value) {
            onChanged(value);
          },
        ),
        title: Text(
          "Heater " + element.toString(),
          style: Theme.of(context)
              .textTheme
              .subtitle1
              .copyWith(color: heaterColor[element]),
        ),
      ));
    });
    return Column(
      children: items,
    );
  }
}
