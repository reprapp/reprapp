import 'dart:math';

import 'package:charts_flutter/flutter.dart' as charts;
import 'package:dac/model/heater_log.dart';
import 'package:dac/model/printer.dart';
import 'package:dac/model/printer_data.dart';
import 'package:dac/services/heaterslog_service.dart';
import 'package:dac/widgets/tools/tools_panel.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:hooks_riverpod/all.dart';
import 'package:intl/intl.dart';

class SegmentsLineChart extends ConsumerWidget {
  final Printer printer;

  SegmentsLineChart(this.printer);

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    Map<int, List<HeaterLogValue>> heaters =
        watch(heatersLogHistoryProvider(printer)).heatersLog;
    var seriesList = _createSeriesList(heaters);

    DateTime viewportEnd = DateTime.now();
    DateTime viewportStart = viewportEnd.subtract(Duration(minutes: 10));

    if (printer != null) {
      if (seriesList.length > 0) {
        return _HeatersLineChart(seriesList, viewportStart, viewportEnd);
      } else {
        return Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Text(printer.name + " " + (printer.enabled ? "" : "is offline")),
              Text("Heaters not available"),
            ]);
      }
    } else {
      return Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Text("No printer"),
            Text("Heaters not available"),
          ]);
    }
  }
}

class SegmentsLineChartStatic extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Map<int, List<HeaterLogValue>> tools = {
      0: _mockHeater(0),
      1: _mockHeater(1)
    };
    var seriesList = _createSeriesList(tools);

    DateTime viewportEnd = DateTime.now();
    DateTime viewportStart = viewportEnd.subtract(Duration(minutes: 10));

    return _HeatersLineChart(seriesList, viewportStart, viewportEnd);
  }
}

List<HeaterLogValue> _mockHeater(int heater) {
  List<HeaterLogValue> records = [];
  for (int i = 0; i < 50; i++) {
    records.add(HeaterLogValue(
        heater: heater,
        state: HeaterState.OFF,
        dateTime: DateTime.now().subtract(Duration(minutes: i)),
        temperatureCurrent:
            _staticFunc((i * (1 + heater)).toDouble(), heater * 50)));
  }
  return records;
}

double _staticFunc(double i, int b) {
  return sin(i) * 7 + 100 - b;
}

List<charts.Series<HeaterLogValue, DateTime>> _createSeriesList(
    Map<int, List<HeaterLogValue>> heatersLogs) {
  List<charts.Series<HeaterLogValue, DateTime>> data = [];

  heatersLogs.forEach((heater, logs) {
    Map<HeaterLine, List<HeaterLogValue>> _heaterLines = {
      HeaterLine.CURRENT: [],
      HeaterLine.TARGET: []
    };
    logs.forEach((heaterLog) {
      if (_heaterVisible(heaterLog.state)) {
        _heaterLines[HeaterLine.CURRENT].add(heaterLog);
        if (heaterLog.state == HeaterState.ACTIVE ||
            heaterLog.state == HeaterState.STANDBY) {
          _heaterLines[HeaterLine.TARGET].add(heaterLog);
        } else {
          _heaterLines[HeaterLine.TARGET].add(heaterLog.copyWith(
              temperatureActive: 0, temperatureStandby: 0, dateTime: null));
        }
      }
    });

    _heaterLines.forEach((line, logs) {
      data.add(_buildSeries(heater, line, logs));
    });
  });
  return data;
}

bool _heaterVisible(HeaterState state) {
  return [HeaterState.ACTIVE, HeaterState.STANDBY, HeaterState.OFF]
      .contains(state);
}

charts.Series _buildSeries(
    int heater, HeaterLine line, List<HeaterLogValue> logs) {
  return new charts.Series<HeaterLogValue, DateTime>(
    id: 'Heater $heater',
    colorFn: (HeaterLogValue value, __) =>
        heatersStyle(line, value).color.shadeDefault,
    dashPatternFn: (HeaterLogValue value, _) =>
        heatersStyle(line, value).dashPattern,
    domainFn: (HeaterLogValue value, _) => value.dateTime,
    measureFn: (HeaterLogValue value, _) => _temperature(value, line),
    data: logs,
  );
}

double _temperature(HeaterLogValue value, HeaterLine line) {
  switch (line) {
    case HeaterLine.CURRENT:
      return value.temperatureCurrent;
    case HeaterLine.TARGET:
      if (value.state == HeaterState.ACTIVE) {
        return value.temperatureActive;
      } else if (value.state == HeaterState.STANDBY) {
        return value.temperatureStandby;
      }
      return 0;
    default:
      return 0;
  }
}

class _HeatersLineChart extends StatelessWidget {
  final List<charts.Series<HeaterLogValue, DateTime>> seriesList;
  final DateTime viewportStart;
  final DateTime viewportEnd;

  _HeatersLineChart(this.seriesList, this.viewportStart, this.viewportEnd);

  @override
  Widget build(BuildContext context) {
    if (seriesList != null && seriesList.length > 0) {
      return charts.TimeSeriesChart(seriesList,
          behaviors: [
            new charts.SlidingViewport(),
            new charts.PanAndZoomBehavior(),
          ],
          animate: false,
          dateTimeFactory: const charts.LocalDateTimeFactory(),
          primaryMeasureAxis: new charts.NumericAxisSpec(
              tickFormatterSpec: charts.BasicNumericTickFormatterSpec(
                  (num value) => temperatureFormat(value, decimals: 0)),
              renderSpec: new charts.GridlineRendererSpec(
                  labelStyle: charts.TextStyleSpec(
                      color: charts.MaterialPalette.white.darker),
                  lineStyle: charts.LineStyleSpec(
                      color: charts.MaterialPalette.white.darker, thickness: 0),
                  axisLineStyle: charts.LineStyleSpec(
                      color: charts.Color.transparent, thickness: 0))),
          domainAxis: new DateTimeAxisSpecWorkaround(
              renderSpec: new charts.GridlineRendererSpec(
                  // labelOffsetFromTickPx: 20,
                  labelOffsetFromAxisPx: 10,
                  tickLengthPx: 0,
                  lineStyle: charts.LineStyleSpec(
                      color: charts.Color.white.darker, thickness: 0),
                  axisLineStyle: charts.LineStyleSpec(
                    color: charts.MaterialPalette.transparent,
                    thickness: 1,
                    dashPattern: null,
                  ),
                  labelStyle: charts.TextStyleSpec(
                      color: charts.MaterialPalette.white.darker),
                  labelAnchor: charts.TickLabelAnchor.centered),
              viewport: charts.DateTimeExtents(
                  start: viewportStart, end: viewportEnd),
              tickFormatterSpec: new charts.AutoDateTimeTickFormatterSpec(
                  minute: new charts.TimeFormatterSpec(format: "HH:mm:ss", transitionFormat: DateFormat.Hm().pattern))));
    } else {
      return Container(
        child: Text("No data"),
      );
    }
  }
}

class DateTimeAxisSpecWorkaround extends charts.DateTimeAxisSpec {
  const DateTimeAxisSpecWorkaround(
      {charts.RenderSpec<DateTime> renderSpec,
      charts.DateTimeTickProviderSpec tickProviderSpec,
      charts.DateTimeTickFormatterSpec tickFormatterSpec,
      bool showAxisLine,
      var viewport})
      : super(
            viewport: viewport,
            renderSpec: renderSpec,
            tickProviderSpec: tickProviderSpec,
            tickFormatterSpec: tickFormatterSpec,
            showAxisLine: showAxisLine);

  @override
  configure(charts.Axis<DateTime> axis, charts.ChartContext context,
      charts.GraphicsFactory graphicsFactory) {
    super.configure(axis, context, graphicsFactory);
    axis.autoViewport = false;
  }
}

class HeaterStyle {
  var color;
  var dashPattern;

  HeaterStyle(this.color, this.dashPattern);
}

enum HeaterLine { CURRENT, TARGET }

HeaterStyle heatersStyle(HeaterLine line, HeaterLogValue value) {
  Map _styles = {
    HeaterLine.CURRENT: [
      HeaterStyle(charts.MaterialPalette.blue, null),
      HeaterStyle(charts.MaterialPalette.red, null),
      HeaterStyle(charts.MaterialPalette.green, null),
      HeaterStyle(charts.MaterialPalette.purple, null),
    ],
    HeaterLine.TARGET: [
      HeaterStyle(charts.MaterialPalette.blue, [4, 4]),
      HeaterStyle(charts.MaterialPalette.red, [4, 4]),
      HeaterStyle(charts.MaterialPalette.green, [4, 4]),
      HeaterStyle(charts.MaterialPalette.purple, [4, 4]),
    ]
  };
  return _styles[line][value.heater];
}

List<Color> heaterColor = [
  Colors.blue,
  Colors.red,
  Colors.green,
  Colors.purple
];
