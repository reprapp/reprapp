import 'package:dac/model/printer.dart';
import 'package:dac/services/printers_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/all.dart';

class PrinterAddPage extends StatelessWidget {
  static const String routeName = '/printer/add';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Theme.of(context).appBarTheme.backgroundColor,
          leading: new IconButton(
            icon: new Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          title: Text("Add Printer"),
        ),
        body: Padding(
          padding: const EdgeInsets.all(16.0),
          child: _AddManualForm(),
        ));
  }
}

class _AddManualForm extends StatefulWidget {
  @override
  _AddManualFormState createState() => _AddManualFormState();
}

class _AddManualFormState extends State<_AddManualForm> {
  final _formKey = GlobalKey<FormState>();
  final _addressController = TextEditingController();
  final _nameController = TextEditingController();

  @override
  void dispose() {
    _addressController.dispose();
    _nameController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    String startPrinterName = "Printer #" +
        (context.read(printersListProvider).count() + 1).toString();
    return Card(
        child: FocusTraversalGroup(
            descendantsAreFocusable: true,
            child: Form(
                key: _formKey,
                autovalidateMode: AutovalidateMode.always,
                child: new ListView(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  children: <Widget>[
                    Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: TextFormField(
                          keyboardType: TextInputType.url,
                          controller: _addressController,
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Please enter the printer address';
                            } else {
                              if (value.startsWith("http")) {
                                return "Enter only the hostname. Don't start with http.\nExample: 192.168.1.10";
                              }
                              Uri _uri = Uri.parse("http://" + value);
                              if (!_uri.isAbsolute) {
                                return "Invalid address";
                              }
                              if (context
                                  .read(printersListProvider)
                                  .exists(hostname: _addressController.text)) {
                                return "Printer already exists with this address";
                              }
                            }
                            return null;
                          },
                          autofocus: true,
                          decoration: InputDecoration(
                            icon: const Icon(Icons.wifi),
                            labelText: 'Address',
                            // border: InputBorder(borderSide: BorderSide(width: 1)),
                          ),
                        )),
                    Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: TextFormField(
                          controller: _nameController..text = startPrinterName,
                          decoration: InputDecoration(
                            icon: const Icon(Icons.book),
                            labelText: 'Name',
                          ),
                        )),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 16.0),
                      child: ElevatedButton(
                        onPressed: () {
                          if (_formKey.currentState.validate()) {
                            PrintersService().addNew(
                                hostname: _addressController.text,
                                name: _nameController.text);
                            ScaffoldMessenger.of(context).showSnackBar(
                                SnackBar(content: Text("Printer added")));
                            Navigator.of(context).pop();
                          }
                        },
                        child: Text('Add'),
                      ),
                    ),
                  ],
                ))));
  }
}

class PrintersListWidget extends ConsumerWidget {
  @override
  Widget build(BuildContext context, ScopedReader watch) {
    List<Printer> printers = watch(printersListProvider).records;

    return ListView.builder(
      itemCount: printers.length,
      itemBuilder: (context, index) {
        final item = printers[index];

        return Dismissible(
          key: Key(item.hostname),
          onDismissed: (direction) => showDeleteDialog(context, item),
          background: Container(
            color: Colors.red,
            child: Text("Delete"),
          ),
          child: ListTile(title: Text('$item')),
        );
      },
    );
  }

  showDeleteDialog(BuildContext context, Printer printer) {
    Widget cancelButton = FlatButton(
      child: Text("Cancel"),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );

    Widget continueButton = FlatButton(
      child: Text("Continue"),
      onPressed: () {
        PrintersService().remove(printer.id);
        ScaffoldMessenger.of(context)
            .showSnackBar(SnackBar(content: Text("$printer removed")));
      },
    );

    AlertDialog alert = AlertDialog(
      title: Text("Remove Printer"),
      content: Text("Do you confirm removing this printer?"),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    showDialog(context: context, builder: (BuildContext context) => alert);
  }
}
