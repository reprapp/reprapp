import 'package:dac/scaffolds/logs_page.dart';
import 'package:dac/scaffolds/printer_page.dart';
import 'package:dac/scaffolds/printers_page.dart';
import 'package:dac/scaffolds/settings_page.dart';
import 'package:dac/scaffolds/space_page.dart';
import 'package:dac/scaffolds/tile_settings_page.dart';
import 'package:dac/scaffolds/webcam_edit_page.dart';
import 'package:dac/scaffolds/webcam_page.dart';
import 'package:dac/scaffolds/webcams_page.dart';
import 'package:dac/services/heaterslog_service.dart';
import 'package:dac/services/printers_data_service.dart';
import 'package:dac/services/printers_service.dart';
import 'package:dac/services/settings_service.dart';
import 'package:dac/services/spaces_service.dart';
import 'package:dac/services/watch/watch_channels.dart';
import 'package:dac/services/webcams_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:theme_provider/theme_provider.dart';

import 'model/space.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SettingsService();
  PrintersService();
  PrintersDataService();
  HeatersLogService();
  WebcamsService();
  WatchChannels();
  runApp(const ProviderScope(child: const MyApp()));
}

bool get isInDebugMode {
  bool inDebugMode = false;
  assert(inDebugMode = true);
  return inDebugMode;
}

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ThemeProvider(
      themes: <AppTheme>[
        AppTheme(
          id: 'light',
          description: "Light",
          data: ThemeData(
            brightness: Brightness.light,
            // accentColor: Colors.blueAccent,
            // toggleableActiveColor: Colors.blue,
            scaffoldBackgroundColor: Color.fromRGBO(250, 250, 250, 1),
            canvasColor: Color.fromRGBO(240, 240, 240, 1),
            // cardColor: Colors.white12,
            selectedRowColor: Color.fromRGBO(240, 240, 240, 1),
            // iconTheme: IconThemeData(color: Colors.white),
          ),
        ),
        AppTheme(
          id: 'dark',
          description: "Dark",
          data: ThemeData(
            brightness: Brightness.dark,
            accentColor: Colors.blueAccent,
            toggleableActiveColor: Colors.blue,
            scaffoldBackgroundColor: Color.fromRGBO(20, 20, 20, 1),
            canvasColor: Color.fromRGBO(30, 30, 30, 1),
            cardColor: Colors.white12,
            selectedRowColor: Color.fromRGBO(30, 30, 30, 1),
            iconTheme: IconThemeData(color: Colors.white),
          ),
        ),
        AppTheme(
          id: 'very_dark',
          description: "Very dark",
          data: ThemeData(
            brightness: Brightness.dark,
            primarySwatch: Colors.grey,
            accentColor: Colors.grey.shade600,
            toggleableActiveColor: Colors.blueGrey,
            scaffoldBackgroundColor: Colors.black,
            canvasColor: Color.fromRGBO(10, 10, 10, 1),
            cardColor: Colors.white10,
            selectedRowColor: Color.fromRGBO(20, 20, 20, 1),
            iconTheme: IconThemeData(color: Colors.white),
            appBarTheme: AppBarTheme(
              backgroundColor: Colors.black,
              foregroundColor: Colors.white12,
              elevation: 0,
            ),
          ),
        ),
      ],
      defaultThemeId: 'dark',
      saveThemesOnChange: true,
      loadThemeOnInit: false,
      onInitCallback: (controller, previouslySavedThemeFuture) async {
        String savedTheme = await previouslySavedThemeFuture;
        if (savedTheme != null) {
          controller.setTheme(savedTheme);
        } else {
          Brightness platformBrightness =
              SchedulerBinding.instance.window.platformBrightness;
          if (platformBrightness == Brightness.dark) {
            controller.setTheme('dark');
          } else {
            controller.setTheme('light');
          }
          controller.forgetSavedTheme();
        }
      },
      child: ThemeConsumer(
        child: Builder(
          builder: (themeContext) => Consumer(
            builder: (context, watch, child) {
              AsyncValue<Space> settings = watch(spaceStoredProvider);
              return settings.when(
                  data: (space) {
                    String initialRoute;
                    if (space != null) {
                      initialRoute = SpacePage.routeName;
                    }
                    return MaterialApp(
                      title: "Duet App Control",
                      debugShowCheckedModeBanner: false,
                      theme: ThemeProvider.themeOf(themeContext).data,
                      home: PrintersScaffold(),
                      initialRoute: initialRoute,
                      routes: {
                        PrintersScaffold.routeName: (c) => PrintersScaffold(),
                        PrinterPage.routeName: (c) => PrinterPage(),
                        WebcamsPage.routeName: (c) => WebcamsPage(),
                        WebcamPage.routeName: (c) => WebcamPage(),
                        WebcamEditPage.routeName: (c) => WebcamEditPage(),
                        TileSettingsPage.routeName: (c) => TileSettingsPage(),
                        SpacePage.routeName: (c) => SpacePage(),
                        LogsPage.routeName: (c) => LogsPage(),
                        SettingsPage.routeName: (c) => SettingsPage(),
                      },
                    );
                  },
                  loading: () => Center(
                        child: SizedBox(
                            height: 200.0,
                            width: 200.0,
                            child: CircularProgressIndicator()),
                      ),
                  error: (_, e) => Center(
                      child: Text("Error: " + e.toString(),
                          textDirection: TextDirection.ltr)));
            },
          ),
        ),
      ),
    );
  }
}
