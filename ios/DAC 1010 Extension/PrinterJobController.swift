//
//  PrinterJobController.swift
//  DAC 1010 Extension
//
//  Created by João Martins on 25/12/2020.
//

import Foundation
import WatchKit

class PrinterJobController: NSObject {
    
    @IBOutlet weak var Filename: WKInterfaceLabel!
    @IBOutlet weak var TimeLeft: WKInterfaceTimer!
    
    private var timer: Timer?
    private var timeLeftSeconds: Int = 0
    
    func setData(data: JobStatus?){
        Filename.setText(data?.filename ?? "-")
        
        timeLeftSeconds = data?.timeLeftSeconds ?? 0
        
        if timeLeftSeconds > 0 {
            DispatchQueue.main.async {
                self.initTimer()
            }
        } else {
            TimeLeft.setDate(Date())
        }
    }
    
    private func initTimer() {
        if timer == nil {
            timer = Timer.scheduledTimer(withTimeInterval: 0.9, repeats: true) {_ in
                self.onTimerFires()
            }
            timer!.tolerance = 0.5
            RunLoop.current.add(timer!, forMode: RunLoop.Mode.common)
        }
    }
    
    @objc func onTimerFires() {
        timeLeftSeconds -= 1
        displayTimerLeft()

        if timeLeftSeconds <= 0 {
            timer?.invalidate()
            timer = nil
        }
    }
    
    private func displayTimerLeft(){
        TimeLeft.setDate(Date.init(timeInterval: TimeInterval.init(timeLeftSeconds),
                                   since: Date()))
    }
    
//    override func didDeactivate() {
//        timer?.invalidate()
//    }
}

