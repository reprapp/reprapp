//
//  ProgressView.swift
//  DAC 1010
//
//  Created by João Martins on 08/01/2021.
//

import SwiftUI

struct ProgressView: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct ProgressView_Previews: PreviewProvider {
    static var previews: some View {
        ProgressView()
    }
}
