//
//  PrinterStatusController.swift
//  DAC 1010 Extension
//
//  Created by João Martins on 25/12/2020.
//

import Foundation
import WatchKit

class PrinterStatusController: NSObject {
    
    @IBOutlet weak var status: WKInterfaceLabel!
    @IBOutlet weak var progress: WKInterfaceGroup!
 
    var imageArray = [UIImage]()
    
    override init() {
        super.init()
        for index in 0...100 {
            let image = UIImage(named: "percentage\(index)i")
            self.imageArray.append(image!)
        }
    }

    func setProgress(value: Int?) {
        self.progress.setBackgroundImage(imageArray[value ?? 0])
    }
    
    func setStatus(value: String?) {
        self.status.setText(value ?? "")
    }

}

