//
//  WatchMessagesConstants.swift
//  Runner
//
//  Created by João Martins on 22/12/2020.
//

import Foundation

class WatchMessagesConstants {

    static let method = "method"
    static let store = "store"
    static let printerId = "printerId"

    static let printersList = "printersList"
    static let printer = "printer"

    static let printerJobStatus = "printerJobStatus"
    static let printerData = "printerData"

    static let flutterMessages = [printersList, printer, printerJobStatus, printerData]
}
